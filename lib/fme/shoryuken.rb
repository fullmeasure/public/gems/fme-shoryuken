# frozen_string_literal: true

require 'rails'
require 'active_support'

module Fme
  module Shoryuken
    extend ActiveSupport::Autoload

    autoload :VERSION
    @queues = []

    def self.add_queues(additional_queues)
      raise 'queue can not be added after initialization' if const_defined?('SHORYUKEN_INITIALIZED') && Fme::Shoryuken::SHORYUKEN_INITIALIZED
      Array.wrap(additional_queues).each do |queue|
        @queues.push(queue)
      end
    end

    def self.queues
      @queues
    end

    class Railtie < Rails::Railtie
      config.after_initialize do
        require 'shoryuken'
        Aws.config.update(region: 'us-east-1')
        Fme::Shoryuken::SHORYUKEN_PREFIX = "shoryuken_#{Rails.app_class.parent.name.underscore}_#{Rails.env}".freeze unless const_defined?('Fme::Shoryuken::SHORYUKEN_PREFIX')
        Fme::Shoryuken::SHORYUKEN_INITIALIZED = true
        Rails.application.config.active_job.queue_adapter = :shoryuken
        Rails.application.config.active_job.queue_name_prefix = SHORYUKEN_PREFIX
        ::Shoryuken.active_job_queue_name_prefixing = true
        # :nocov:
        ::Shoryuken.configure_server do |_config|
          Rails.logger = ::Shoryuken::Logging.logger
          Rails.logger.level = Rails.application.config.log_level

          ::Shoryuken.sqs_client_receive_message_opts = { wait_time_seconds: 20 }
          ::Shoryuken.options[:delay] = 25
          ::Shoryuken.add_group('dynamic', 10)
          Fme::Shoryuken.queues.each do |name|
            queue_name = "#{SHORYUKEN_PREFIX}_#{name}"
            dead_letter_queue_name = "#{queue_name}_failures"

            ::Shoryuken.sqs_client.create_queue(queue_name: dead_letter_queue_name)
            dead_letter_queue_url = ::Shoryuken.sqs_client.get_queue_url(queue_name: dead_letter_queue_name).queue_url
            dead_letter_queue_arn = ::Shoryuken.sqs_client.get_queue_attributes(queue_url: dead_letter_queue_url, attribute_names: ['QueueArn']).attributes['QueueArn']

            ::Shoryuken.sqs_client.create_queue(queue_name: queue_name)
            queue_url = ::Shoryuken.sqs_client.get_queue_url(queue_name: queue_name).queue_url
            ::Shoryuken.sqs_client.set_queue_attributes(queue_url: queue_url,
                                                        attributes: {
                                                          'RedrivePolicy' => {
                                                            'maxReceiveCount' => '5',
                                                            'deadLetterTargetArn' => dead_letter_queue_arn
                                                          }.to_json
                                                        })

            ::Shoryuken.add_queue(queue_name, 1, 'dynamic')
          end
        end
        # :nocov:
      end
    end
  end
end
