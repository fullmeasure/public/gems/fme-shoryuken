$:.push File.expand_path('lib', __dir__)

# Maintain your gem's version:
require 'fme/shoryuken/version'

# Describe your gem and declare its dependencies:
Gem::Specification.new do |spec|
  spec.name        = 'fme-shoryuken'
  spec.version     = Fme::Shoryuken::VERSION
  spec.authors     = ['Full Measure Education']
  spec.email       = ['devops@fullmeasureed.com']
  spec.homepage    = 'https://gitlab.com/fullmeasure/public/gems/fme-shoryuken'
  spec.summary     = 'A ruby library for initializing the shoryuken gem for fme use'
  spec.description = 'A ruby library for initializing the shoryuken gem for fme use'
  spec.license     = 'MIT'

  spec.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]

  spec.add_dependency 'activesupport'
  spec.add_dependency 'rails', '~> 5'
  spec.add_dependency 'shoryuken', '~> 3'
  spec.add_dependency 'aws-sdk-sqs', '~> 1'
  spec.add_dependency 'aws-sdk-sns', '~> 1'

  spec.add_development_dependency 'bundler', '~> 1'
  spec.add_development_dependency 'pry'
  spec.add_development_dependency 'pry-byebug'
  spec.add_development_dependency 'rspec', '~> 3'
  spec.add_development_dependency 'simplecov', '~> 0'
  spec.add_development_dependency 'webmock', '~> 3'
end
