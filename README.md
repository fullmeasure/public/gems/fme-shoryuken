# Fme::Shoryuken
This is a library intended to make it easy use the shoryuken gem.

## Installation
Add this line to your application's Gemfile:

```ruby
gem 'fme-shoryuken'
```

And then execute:
```bash
$ bundle
```

Or install it yourself as:
```bash
$ gem install fme-shoryuken
```

## Usage
### initializer
fme-shoryuken.rb
```ruby
Fme::Shoryuken::SHORYUKEN_PREFIX = 'optional override prefix goes here'
Fme::Shoryuken::QUEUES = %w()
```

## License
The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
