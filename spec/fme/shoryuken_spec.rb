require 'rails_helper'

RSpec.describe Fme::Shoryuken do
  it "has a version number" do
    expect(Fme::Shoryuken::VERSION).not_to be nil
  end

  describe 'add_queues' do
    let!(:prefix) { Fme::Shoryuken::SHORYUKEN_PREFIX }
    let!(:initialized) { Fme::Shoryuken::SHORYUKEN_INITIALIZED }
    before(:each) do
      Fme::Shoryuken.send(:remove_const, :SHORYUKEN_PREFIX)
      Fme::Shoryuken.send(:remove_const, :SHORYUKEN_INITIALIZED)
    end
    after(:each) do
      Fme::Shoryuken::SHORYUKEN_PREFIX = prefix
      Fme::Shoryuken::SHORYUKEN_INITIALIZED = initialized
    end
    it 'is added to the queue' do
      Fme::Shoryuken.add_queues(['new_queue'])
      expect(Fme::Shoryuken.queues).to include('new_queue')
    end
  end

  describe 'config' do
    it { expect(Fme::Shoryuken::SHORYUKEN_PREFIX).to eq "shoryuken_#{Rails.app_class.parent.name.underscore}_#{Rails.env}" }
    it { expect(Rails.application.config.active_job.queue_adapter).to eq :shoryuken }
    it { expect(Rails.application.config.active_job.queue_name_prefix).to eq Fme::Shoryuken::SHORYUKEN_PREFIX }
    it { expect(::Shoryuken.active_job_queue_name_prefixing).to eq true }
  end
end
